import distances 

def test_manhattan_example():
    u = [2, -1]
    v = [-2, 2]

    assert distances.manhattan_distance(u, v) == 7
