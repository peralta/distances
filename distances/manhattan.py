import math 

def manhattan_distance(u, v):
    """
    Computes the Manhattan distance between two vectors `u` and `v`.

    The Euclidean distance between `u` and `v`, is defined as:

    |u_1 - v_1| + ... + |u_n - v_n|

    Parameters
    ----------
    u : list
        Input vector.
    v : list
        Input vector.

    Returns
    -------
    manhattan : double
        The Manhattan distance between vectors `u` and `v`.
    """
    distance = 0

    for u_i, v_i in zip(u, v):
        distance += abs(u_i - v_i)

    return distance