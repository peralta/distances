import math

def euclidean_distance(vector_one, vector_two):
    #this function returns the euclidean 
    # distance beteween two vectors 
    """
    Computes the Euclidean distance between two vectos `u` and `v`.

    The Euclidean distance between `u` and `v`, is defined as:

    \sqrt{(u_1 - v_1) ^ 2 + ... + (u_n - v_n) ^ 2}

    Parameters
    ----------
    u : list
        Input vector.
    v : list
        Input vector.

    Returns
    -------
    euclidean : double
        The Euclidean distance between vectors `u` and `v`.
    """
    dimension = len(vector_one)
    distance = 0
    for index in range(dimension):
        distance += (vector_one[index] - vector_two[index]) ** 2
    
    return math.sqrt(distance)