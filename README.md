# distances

A python package for calculating various distance measures. 

# Instances 
The project can be cloned locally using the following command:

```
$ git clone <path>
```

# Usage

Currently the following distance measures are implemented in the package:

*Euclidean distance. 
$D_\mathrm{euclidean} = /sum_i {u_i - v_i)^2$}

## Tests

The package is being tested using `pytest`. To run the test suite use the
command:

```
$ pytest test_euclidean
```

## Licence 
The package is under the MIT license.

